//
// Created by Professional on 09.10.2020.
//

#ifndef OS_LAB1_UTILS_H
#define OS_LAB1_UTILS_H

#include <iostream>
#include <conio.h>
#include <stdlib.h>

void clsAndContinue() {
    std::cout << "press any key to continue...";
    getch();
    system("cls");
}

#endif //OS_LAB1_UTILS_H
