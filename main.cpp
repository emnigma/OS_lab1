#include <iostream>
#include "menu.h"

using namespace std;

void showMainOptions() {
    cout << "choose option:\n";
    cout << "0 - list drives\n";
    cout << "1 - go to disk information menu\n";
    cout << "2 - go to create/delete directories menu\n";
    cout << "3 - go to create file menu\n";
    cout << "4 - go to copy/move menu\n";
    cout << "5 - go to attributes menu\n";
    cout << "6 - terminate program\n";
    std::cout << "option -> ";
}

int getMenuOption(int i) {

    switch( i ) {
        case 0:
            listDrives();
            clsAndContinue();
            break;
        case 1:
            showGetDiskInfoMenu();
            clsAndContinue();
            break;
        case 2:
            showCreateDeleteDirMenu();
            clsAndContinue();
            break;
        case 3:
            showCreateFileMenu();
            clsAndContinue();
            break;
        case 4:
            showCopyMoveMenu();
            clsAndContinue();
            break;
        case 5:
            showAttributesChangeMenu();
            clsAndContinue();
            break;
        case 6:
            break;
        default:
            i = -1;
            break;
    }
    return 0;
}

int main() {
    int i = -1;
    while (i != 6) {
        showMainOptions();
        cin >> i;
        getMenuOption(i);
    }

}
