//
// Created by Professional on 17.09.2020.
//

#ifndef OS_LAB1_FILESYSTEM_MANAGING_H
#define OS_LAB1_FILESYSTEM_MANAGING_H

#include <windows.h>
#include <vector>
#include <string>
#include <map>

#define UNKNOWN "NO DATA"

typedef std::map<std::string, std::string> dict;


std::vector<std::string> getDrives() {
    std::vector<std::string> drives;

    char szLogicalDrives[MAX_PATH];
    DWORD dwResult = GetLogicalDriveStrings(MAX_PATH, szLogicalDrives);

    if (dwResult) {
        char *item = szLogicalDrives; //указатель на первый символ результата(на первую строку из результата)
        while (*item) {
            drives.push_back(item);
            // strlen читает символы до нулевого, возвращает длину полученной строки
            item += strlen(item) + 1;
        }
        return drives;
    }
    else {
        throw std::invalid_argument("NO DRIVES DETECTED");
    }
}

void listDrives() {
    std::vector<std::string> drives = getDrives();
    for (const auto &i : drives) {
        std::cout << i << '\n';
    }
}

std::tuple<dict, std::vector<std::string>> getDriveInfo(const char* Drive) {
    dict disk_info {
        {"DriveName", Drive},
        {"DriveType", ""},
        {"VolumeName", ""},
        {"SystemName", ""},
        {"SerialNumber", ""},
        {"FreeSpace", ""}
    };
    std::string DriveType;
    switch ( GetDriveType(Drive) ) {
        case 0:
            DriveType = "DRIVE_UNKNOWN";
            break;
        case 1:
            DriveType = "DRIVE_NO_ROOT_DIR";
            break;
        case 2:
            DriveType = "DRIVE_REMOVABLE";
            break;
        case 3:
            DriveType = "DRIVE_FIXED";
            break;
        case 4:
            DriveType = "DRIVE_REMOTE";
            break;
        case 5:
            DriveType = "DRIVE_CDROM";
            break;
        case 6:
            DriveType = "DRIVE_RAMDISK";
            break;
    }
    disk_info["DriveType"] = DriveType;
    std::vector<std::string> disk_fsflags;

    char VolumeNameBuffer[MAX_PATH];
    char SysNameBuffer[MAX_PATH];
    DWORD VolumeSerialNumber;
    DWORD MaxComponentLength;
    DWORD FileSystemFlags;
    if (GetVolumeInformation(Drive,
                             VolumeNameBuffer,
                             sizeof(VolumeNameBuffer),
                             &VolumeSerialNumber,
                             &MaxComponentLength,
                             &FileSystemFlags,
                             SysNameBuffer,
                             sizeof(SysNameBuffer)))
    {
        disk_info["VolumeName"] = VolumeNameBuffer;
        disk_info["SystemName"] = SysNameBuffer;
        disk_info["SerialNumber"] = std::to_string(VolumeSerialNumber);
        if (FileSystemFlags & FILE_CASE_PRESERVED_NAMES)
            disk_fsflags.push_back("FILE_CASE_PRESERVED_NAMES");
        if (FileSystemFlags & FILE_CASE_SENSITIVE_SEARCH)
            disk_fsflags.push_back("FILE_CASE_SENSITIVE_SEARCH");
        if (FileSystemFlags & FILE_FILE_COMPRESSION)
            disk_fsflags.push_back("FILE_FILE_COMPRESSION");
        if (FileSystemFlags & FILE_NAMED_STREAMS)
            disk_fsflags.push_back("FILE_NAMED_STREAMS");
        if (FileSystemFlags & FILE_PERSISTENT_ACLS)
            disk_fsflags.push_back("FILE_PERSISTENT_ACLS");
        if (FileSystemFlags & FILE_READ_ONLY_VOLUME)
            disk_fsflags.push_back("FILE_READ_ONLY_VOLUME");
        if (FileSystemFlags & FILE_SEQUENTIAL_WRITE_ONCE)
            disk_fsflags.push_back("FILE_SEQUENTIAL_WRITE_ONCE");
        if (FileSystemFlags & FILE_SUPPORTS_ENCRYPTION)
            disk_fsflags.push_back("FILE_SUPPORTS_ENCRYPTION");
        if (FileSystemFlags & FILE_SUPPORTS_EXTENDED_ATTRIBUTES)
            disk_fsflags.push_back("FILE_SUPPORTS_EXTENDED_ATTRIBUTES");
        if (FileSystemFlags & FILE_SUPPORTS_HARD_LINKS)
            disk_fsflags.push_back("FILE_SUPPORTS_HARD_LINKS");
        if (FileSystemFlags & FILE_SUPPORTS_OBJECT_IDS)
            disk_fsflags.push_back("FILE_SUPPORTS_OBJECT_IDS");
        if (FileSystemFlags & FILE_SUPPORTS_OPEN_BY_FILE_ID)
            disk_fsflags.push_back("FILE_SUPPORTS_OPEN_BY_FILE_ID");
        if (FileSystemFlags & FILE_SUPPORTS_REPARSE_POINTS)
            disk_fsflags.push_back("FILE_SUPPORTS_REPARSE_POINTS");
        if (FileSystemFlags & FILE_SUPPORTS_SPARSE_FILES)
            disk_fsflags.push_back("FILE_SUPPORTS_SPARSE_FILES");
        if (FileSystemFlags & FILE_SUPPORTS_TRANSACTIONS)
            disk_fsflags.push_back("FILE_SUPPORTS_TRANSACTIONS");
        if (FileSystemFlags & FILE_SUPPORTS_USN_JOURNAL)
            disk_fsflags.push_back("FILE_SUPPORTS_USN_JOURNAL");
        if (FileSystemFlags & FILE_UNICODE_ON_DISK)
            disk_fsflags.push_back("FILE_UNICODE_ON_DISK");
        if (FileSystemFlags & FILE_VOLUME_IS_COMPRESSED)
            disk_fsflags.push_back("FILE_VOLUME_IS_COMPRESSED");
        if (FileSystemFlags & FILE_VOLUME_QUOTAS)
            disk_fsflags.push_back("FILE_VOLUME_QUOTAS");
    }
    else {
        disk_info["VolumeName"] = UNKNOWN;
        disk_info["SystemName"] = UNKNOWN;
        disk_info["SerialNumber"] = UNKNOWN;
    }

    DWORD SectorsPerCluster;
    DWORD BytesPerSector;
    DWORD NumberOfFreeClusters;
    DWORD TotalNumberOfClusters;
    if (GetDiskFreeSpace(Drive, &SectorsPerCluster, &BytesPerSector, &NumberOfFreeClusters, &TotalNumberOfClusters))
    {
        disk_info["FreeSpace"] = std::to_string(NumberOfFreeClusters * SectorsPerCluster * BytesPerSector);
    }
    else {
        disk_info["FreeSpace"] = UNKNOWN;
    }

    return {disk_info, disk_fsflags};
}

void displayDriveInfo(dict disk_info, std::vector<std::string> disk_fsflags) {

    std::cout << "DriveName:    " << disk_info["DriveName"] << '\n';
    std::cout << "DriveType:    " << disk_info["DriveType"] << '\n';
    std::cout << "FreeSpace:    " << disk_info["FreeSpace"];
    if (disk_info["FreeSpace"] != UNKNOWN) {
        std::cout << " bytes";
    }
    std::cout << '\n';
    std::cout << "SystemName:   " << disk_info["SystemName"] << '\n';
    std::cout << "VolumeName:   " << disk_info["VolumeName"] << '\n';
    std::cout << "SerialNumber: " << disk_info["SerialNumber"] << '\n';
    std::cout << "Disk filesystem attributes:" << '\n';
    if (disk_fsflags.empty()) {
        std::cout << "No filesystem flags acquired\n";
    }
    else {
        for (const auto &i: disk_fsflags) {
            std::cout << i << '\n';
        }
    }
}

bool createFile(const char* path) {
    HANDLE hFile = CreateFile(
            path,
            GENERIC_READ,
            0,
            NULL,
            CREATE_NEW,
            FILE_ATTRIBUTE_NORMAL,
            NULL);

    if (hFile == INVALID_HANDLE_VALUE) {
        CloseHandle(hFile);
        return 0;
    }
    CloseHandle(hFile);
    return 1;
}

bool copyFile(const char* oldPath, const char* newPath) {
    // если файл существует, то перемещения с перезаписываением не произойдет
    return (CopyFile(oldPath, newPath, true));
}

bool mvFile(const char* oldPath, const char* newPath) {
    // перемещение между разными томами симулируется
    return (MoveFileEx(oldPath, newPath, MOVEFILE_COPY_ALLOWED));
}

std::vector<std::string> getFileAttributes(const char* path) {
    DWORD dwAttrs = GetFileAttributes(path);
    if (dwAttrs == 4294967295) {
        throw std::invalid_argument("File does not exist");
    }
    std::vector<std::string> attrs;

    if (dwAttrs & FILE_ATTRIBUTE_ARCHIVE)
        attrs.push_back("ARCHIVE");
    if (dwAttrs & FILE_ATTRIBUTE_COMPRESSED)
        attrs.push_back("COMPRESSED");
    if (dwAttrs & FILE_ATTRIBUTE_DEVICE)
        attrs.push_back("DEVICE");
    if (dwAttrs & FILE_ATTRIBUTE_DIRECTORY)
        attrs.push_back("DIRECTORY");
    if (dwAttrs & FILE_ATTRIBUTE_ENCRYPTED)
        attrs.push_back("ENCRYPTED");
    if (dwAttrs & FILE_ATTRIBUTE_HIDDEN)
        attrs.push_back("HIDDEN");
    if (dwAttrs & FILE_ATTRIBUTE_NORMAL)
        attrs.push_back("NORMAL");
    if (dwAttrs & FILE_ATTRIBUTE_NOT_CONTENT_INDEXED)
        attrs.push_back("NOT_CONTENT_INDEXED");
    if (dwAttrs & FILE_ATTRIBUTE_OFFLINE)
        attrs.push_back("OFFLINE");
    if (dwAttrs & FILE_ATTRIBUTE_READONLY)
        attrs.push_back("READONLY");
    if (dwAttrs & FILE_ATTRIBUTE_REPARSE_POINT)
        attrs.push_back("REPARSE_POINT");
    if (dwAttrs & FILE_ATTRIBUTE_SPARSE_FILE)
        attrs.push_back("SPARSE_FILE");
    if (dwAttrs & FILE_ATTRIBUTE_SYSTEM)
        attrs.push_back("SYSTEM");
    if (dwAttrs & FILE_ATTRIBUTE_TEMPORARY)
        attrs.push_back("TEMPORARY");
    if (dwAttrs & FILE_ATTRIBUTE_ROMSTATICREF)
        attrs.push_back("ROMSTATICREF")
    return attrs;
}

void displayFileAttributes(const char* path) {
    try {
        for (const auto &i : getFileAttributes(path)) {
            std::cout << i << '\n';
        }
    }
    catch(std::invalid_argument &e) {
        std::cout << "This file does not exist\n";
    }
}

void setFileAttributes(const char* path, std::string &attr) {
    DWORD dwAttrs = GetFileAttributes(path);

    if (attr == "ARCHIVE")
        if (!(dwAttrs & FILE_ATTRIBUTE_ARCHIVE))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_ARCHIVE);

    if (attr == "DIRECTORY")
        if (!(dwAttrs & FILE_ATTRIBUTE_DIRECTORY))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_DIRECTORY);

    if (attr == "DEVICE")
        if (!(dwAttrs & FILE_ATTRIBUTE_DEVICE))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_DEVICE);

    if (attr == "COMPRESSED")
        if (!(dwAttrs & FILE_ATTRIBUTE_COMPRESSED))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_COMPRESSED);

    if (attr == "READONLY")
        if (!(dwAttrs & FILE_ATTRIBUTE_READONLY))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_READONLY);

    if (attr == "HIDDEN")
        if (!(dwAttrs & FILE_ATTRIBUTE_HIDDEN))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_HIDDEN);

    if (attr == "NORMAL")
        if (!(dwAttrs & FILE_ATTRIBUTE_NORMAL))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_NORMAL);

    if (attr == "SYSTEM")
        if (!(dwAttrs & FILE_ATTRIBUTE_SYSTEM))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_SYSTEM);

    if (attr == "TEMPORARY")
        if (!(dwAttrs & FILE_ATTRIBUTE_TEMPORARY))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_TEMPORARY);

    if (attr == "ENCRYPTED")
        if (!(dwAttrs & FILE_ATTRIBUTE_ENCRYPTED))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_ENCRYPTED);
 
    if (attr == "ROMSTATICREF")
        if (!(dwAttrs & FILE_ATTRIBUTE_ROMSTATICREF))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_ROMSTATICREF;

    if (attr == "NOT_CONTENT_INDEXED")
        if (!(dwAttrs & FILE_ATTRIBUTE_NOT_CONTENT_INDEXED))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_NOT_CONTENT_INDEXED);

    if (attr == "OFFLINE")
        if (!(dwAttrs & FILE_ATTRIBUTE_OFFLINE))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_OFFLINE);

    if (attr == "REPARSE_POINT")
        if (!(dwAttrs & FILE_ATTRIBUTE_REPARSE_POINT))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_REPARSE_POINT);

    if (attr == "SPARSE_FILE")
        if (!(dwAttrs & FILE_ATTRIBUTE_SPARSE_FILE))
            SetFileAttributes(path, dwAttrs | FILE_ATTRIBUTE_SPARSE_FILE);
    
}

void getInfoByHandle() {
    std::cout << "NOT IMPLEMENTED";
}

dict getFileTime(const char* path) {
    dict times;

    HANDLE hFile = CreateFile(
            path,
            GENERIC_READ,
            0,
            NULL,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            NULL);

    if (hFile == INVALID_HANDLE_VALUE) {
        CloseHandle(hFile);
        throw std::invalid_argument("Invalid handle value");
    }

    FILETIME CreationTime;
    FILETIME LastAccessTime;
    FILETIME LastWriteTime;
    if (GetFileTime(hFile, &CreationTime, &LastAccessTime, &LastWriteTime)) {
        SYSTEMTIME cm, ca, cw;
        if (FileTimeToSystemTime(&CreationTime, &cm) && FileTimeToSystemTime(&LastAccessTime, &ca) && FileTimeToSystemTime(&LastWriteTime, &cw)) {
            std::cout << cm.wYear << " " << cm.wMonth << std::endl;
            times["CreationTime"] = std::to_string(cm.wYear) + "." + std::to_string(cm.wMonth) + "." + std::to_string(cm.wHour) + ":" + std::to_string(cm.wMinute);
            times["LastAccessTime"] = std::to_string(ca.wYear) + "." + std::to_string(ca.wMonth) + "." + std::to_string(ca.wHour) + ":" + std::to_string(ca.wMinute);
            times["LastWriteTime"] = std::to_string(cw.wYear) + "." + std::to_string(cw.wMonth) + "." + std::to_string(cw.wHour) + ":" + std::to_string(cw.wMinute);
        }
        else
            throw std::invalid_argument("Time conversion error");
    }
    else {
        throw std::invalid_argument("Error on gettime");
    }

    CloseHandle(hFile);
    return times;
}

void setFileTime(const char* path) {
    FILETIME fileTime;
    SYSTEMTIME systemTimeNow;
    GetSystemTime(&systemTimeNow);
    SystemTimeToFileTime(&systemTimeNow, &fileTime);

    HANDLE hFile = CreateFile(path, FILE_WRITE_ATTRIBUTES, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (SetFileTime(hFile, &fileTime, NULL, NULL))
        std::cout << "Time set\n"<<systemTimeNow.wDay<<"." << systemTimeNow.wMonth << "."
             << systemTimeNow.wYear << " " << systemTimeNow.wHour << ":" << systemTimeNow.wMinute << "\n";
    else
        std::cout << "Set time Error\n";

    CloseHandle(hFile);
}

#endif //OS_LAB1_FILESYSTEM_MANAGING_H
