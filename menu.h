//
// Created by Professional on 18.09.2020.
//

#ifndef OS_LAB1_MENU_H
#define OS_LAB1_MENU_H

#include "filesystem_managing.h"
#include "utils.h"


void showGetDiskInfoMenu() {
    std::cout << "Choose drive from drives below:\n";
    listDrives();
    std::string drive;
    std::cin >> drive;
    auto [disk_info, disk_fs_info] = getDriveInfo(drive.c_str());
    displayDriveInfo(
            disk_info,
            disk_fs_info
    );
}

void showCreateDeleteDirMenu() {
    std::cout << "Type 1 for create, 2 - for delete directory:\n";
    int choise;
    std::cin >> choise;
    if (choise == 1) {
        std::cout << "Enter directory path to create:\n";
        std::string dir;
        std::cin >> dir;
        if (CreateDirectory(dir.c_str(), nullptr))
            std::cout << "directory successfully created" << '\n';
        else
            std::cout << "error occurred while directory creating" << '\n';
    }
    else if (choise == 2) {
        std::cout << "Enter directory path to delete:\n";
        std::string dir;
        std::cin >> dir;
        if (RemoveDirectory(dir.c_str()))
            std::cout << "directory successfully deleted" << '\n';
        else
            std::cout << "error occurred while directory deleting" << '\n';
    }
    else {
        std::cout << "ERROR";
    }
}

void showCreateFileMenu() {
    std::cout << "enter path to new file:" << '\n';
    std::string path;
    std::cin >> path;
    if (createFile(path.c_str())) {
        std::cout << "file successfully created" << '\n';
    }
    else {
        std::cout << "error occurred while file creating" << '\n';
    }
}

void showCopyMoveMenu() {
    std::cout << "Type 1 for copy, 2 - for move:\n";
    int choise;
    std::cin >> choise;
    if (choise == 1) {
        std::cout << "Enter existing path:\n";
        std::string old_path;
        std::cin >> old_path;
        std::cout << "Enter new path:\n";
        std::string new_path;
        std::cin >> new_path;
        if (old_path == new_path) {
            std::cout << "Enter different paths\n";
        }
        else if (copyFile(old_path.c_str(), new_path.c_str())) {
            std::cout << "file successfully copied" << '\n';
        }
        else {
            std::cout << "error occurred while file copying" << '\n';
        }
    }
    else if (choise == 2) {
        std::cout << "Enter existing path:\n";
        std::string old_path;
        std::cin >> old_path;
        std::cout << "Enter new path:\n";
        std::string new_path;
        std::cin >> new_path;
        if (old_path == new_path) {
            std::cout << "Enter different paths\n";
        }
        else if (mvFile(old_path.c_str(), new_path.c_str())) {
            std::cout << "file successfully moved" << '\n';
        }
        else {
            std::cout << "error occurred while file moving" << '\n';
        }
    }
    else {
        std::cout << "ERROR";
    }
}

// ---------------- { HELP SUBMENUS } ----------------

void showGetAttributesMenu() {
    std::cout << "Enter path to file:\n";
    std::string path;
    std::cin >> path;
    displayFileAttributes(path.c_str());
}

void displayAllAttributes() {
    std::cout <<
        "Chose and enter attribute:\n"
        "ARCHIVE" << '\n' <<
        "COMPRESSED" << '\n' <<
        "DEVICE" << '\n' <<
        "DIRECTORY" << '\n' <<
        "ENCRYPTED" << '\n' <<
        "HIDDEN" << '\n' <<
        "NORMAL" << '\n' <<
        "NOT_CONTENT_INDEXED" << '\n' <<
        "OFFLINE" << '\n' <<
        "READONLY" << '\n' <<
        "REPARSE_POINT" << '\n' <<
        "SPARSE_FILE" << '\n' <<
        "SYSTEM" << '\n' <<
        "TEMPORARY" << '\n';
}

void showSetAttributesMenu() {
    std::cout << "Enter path to file:\n";
    std::string path;
    std::cin >> path;
    displayAllAttributes();
    std::string attr;
    std::cin >> attr;
    setFileAttributes(path.c_str(), attr); // no feedback
}

void showGetFiletimeMenu() {
    std::cout << "Enter path to file:\n";
    std::string path;
    std::cin >> path;
    dict times = getFileTime(path.c_str());
    std::cout << "Created:    ";
    std::cout << times["CreationTime"] << '\n';
    std::cout << "LastAccess: ";
    std::cout << times["LastAccessTime"] << '\n';
    std::cout << "LastWrite:  ";
    std::cout << times["LastWriteTime"] << '\n';
}

void showSetFiletimeMenu() {
    std::cout << "Enter path to file:\n";
    std::string path;
    std::cin >> path;
    setFileTime(path.c_str());
}

void showAttributesChangeMenu() {
    std::cout << "choose option:\n";
    std::cout << "0 - get file attributes\n";
    std::cout << "1 - set file attributes\n";
    std::cout << "2 - get file time\n";
    std::cout << "3 - set file time\n";
    std::cout << "option -> ";

    int choise;
    std::cin >> choise;
    switch ( choise ) {
        case 0:
            showGetAttributesMenu();
            break;
        case 1:
            showSetAttributesMenu();
            break;
        case 2:
            showGetFiletimeMenu();
            break;
        case 3:
            showSetFiletimeMenu();
            break;
        default:
            break;
    }
}

#endif //OS_LAB1_MENU_H
